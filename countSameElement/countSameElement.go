package main

import "fmt"

func main() {
	arr := [14]string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	arr1 := [14]string{"a", "b", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}

	fmt.Println(counting(arr))
	fmt.Println(counting(arr1))
}

func counting(input [14]string) []interface{} {
	var output []interface{}
	var output_temp []interface{}
	var value int

	for i := 0; i < len(input); i++ {
		if i == 0 {
			value++
		}
		if i != 0 && input[i] == input[i-1] {
			value++
		}
		if i != 0 && (input[i] != input[i-1] || i == len(input)-1) {
			output_temp = append(output_temp, value)
			output_temp = append(output_temp, input[i-1])
			output = append(output, output_temp)
			output_temp = nil
			value = 1
		}

	}

	return output
}
