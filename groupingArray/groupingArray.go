package main

import "fmt"

func main() {
	arr := [14]string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	arr1 := [14]string{"a", "b", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}

	fmt.Println(grouping(arr))
	fmt.Println(grouping(arr1))
}

func grouping(input [14]string) [][]string {
	var output [][]string
	var output_temp []string

	output_temp = append(output_temp, input[0])

	for i := 1; i < len(input); i++ {
		if input[i] == input[i-1] {
			output_temp = append(output_temp, input[i])
		}
		if input[i] != input[i-1] || i == len(input)-1 {
			output = append(output, output_temp)
			output_temp = nil
			output_temp = append(output_temp, input[i])
		}
	}
	return output
}
