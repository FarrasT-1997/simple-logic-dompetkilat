package main

import "fmt"

func main() {
	fmt.Println(isPalindrome("abcba"))
	fmt.Println(isPalindrome("test"))
}

func isPalindrome(sentence string) bool {
	var terbalik string
	for i := len(sentence) - 1; i >= 0; i-- {
		terbalik += string(sentence[i])
	}
	if terbalik == sentence {
		return true
	}
	return false
}
