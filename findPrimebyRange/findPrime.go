package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(findPrimeByRange(11, 40))
	fmt.Println(findPrimeByRange(1, 40))
}

func findPrimeByRange(min, max int) []int {
	var output []int

	for i := min; i <= max; i++ {
		if i == 2 || i == 3 || i == 5 || i == 7 {
			output = append(output, i)
			continue
		}
		if i%2 != 0 && i%3 != 0 && i%5 != 0 && i%7 != 0 && i%int(math.Sqrt(float64(i))) != 0 {
			output = append(output, i)
		}
	}
	return output
}
